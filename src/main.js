import Vue from 'vue';
import Vuex from 'vuex'
import App from './App.vue';
import router from './router';
import store from './store';
import axios from 'axios';
import qs from 'qs';
import three from 'three';
import './plugins/element.js';

import En from '@/translation/translationEn'
import Cn from '@/translation/translationCn'

import ElementUI from 'element-ui';
import i18n from '@/translation/i18n/i18n.js';
Vue.use(ElementUI,{
    i18n:(key, value) => i18n.t(key, value)
})

Vue.config.productionTip = false;
Vue.prototype.axios = axios;
Vue.prototype.$qs = qs;

i18n.locale = 'cn'

new Vue({
    router,
    store,
    i18n,
    render: h => h(App),
}).$mount('#app');
